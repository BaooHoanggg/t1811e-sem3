﻿using LINQwithASP.NET.Context;
using LINQwithASP.NET.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace LINQwithASP.NET.Controllers
{
    public class ProductController : Controller
    {
        ProductContext db = new ProductContext();

        // GET: Product
        public ActionResult Index(string sortOrder, string searchString, string sortDate)
        {
            //step 1 request
            //step 2 call model
            //step 3 return view from controller
            ViewBag.NameSort = String.IsNullOrEmpty(sortOrder) ? "name_desc" : "";
            ViewBag.PriceSort = sortOrder == "Price" ? "price_desc" : "Price";
            ViewBag.QtySort = sortOrder == "Qty" ? "qty_desc" : "Qty";
            //ViewBag.DateSort = sortDate == "date_2010" ? "date_2020" : "date_2010";          

            var products = from p in db.Products select p;
            //var dates = from d in db.Products select d;

            if (!String.IsNullOrEmpty(searchString))
            {
                products = products.Where(p => p.Name.Contains(searchString));
            }

            switch (sortOrder)
            {
                case "name_desc": products = products.OrderByDescending(p => p.Name);
                    break;
                case "Price": products = products.OrderBy(p => p.Price);
                    break;
                case "price_desc": products = products.OrderByDescending(p => p.Price);
                    break;
                case "Qty": products = products.OrderBy(s => s.Qty);
                    break;
                case "qty_desc": products = products.OrderByDescending(s => s.Qty);
                    break;
                default: products = products.OrderBy(p => p.Name);
                    break;
            }
            
            //switch (sortDate)
            //{
            //    case "date_2020":
            //        dates = dates.Where(d => d.EntryDate >= new DateTime(2020, 1, 1));
            //        break;
            //    case "date_2010":
            //        dates = dates.Where(d => d.EntryDate >= new DateTime(2010, 1, 1));
            //        break;
            //    default:
            //        dates = dates.OrderByDescending(d => d.EntryDate);
             //       break;
            //}
            
            //var products = db.Products.ToList();
            return View(products.ToList());
        }

        public ActionResult sortEntryDate(string sortDate)
        {
            ViewBag.DateSort = sortDate == "date_2010" ? "date_2020" : "date_2010";
            var dates = from d in db.Products select d;

            switch (sortDate)
            {
                case "date_2020":
                    dates = dates.Where(d => d.EntryDate >= new DateTime(2020, 1, 1))
                                .OrderBy(d => d.EntryDate);
                    break;
                case "date_2010":
                    dates = dates.Where(d => d.EntryDate >= new DateTime(2010, 1, 1))
                                .OrderBy(d => d.EntryDate);
                    break;
                default:
                    dates = dates.OrderByDescending(d => d.EntryDate);
                    break;
            }

            return View(dates.ToList());
        }

        // GET: Product/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

        // GET: Product/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Product/Create
        [HttpPost]
        public ActionResult Create(Product product)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    db.Products.Add(product);
                    db.SaveChanges();
                    return RedirectToAction("Index");
                }
                // TODO: Add insert logic here

                return View(product);
            }
            catch
            {
                return View(product);
            }
        }

        // GET: Product/Edit/5
        public ActionResult Edit(int id)
        {
            return View();
        }

        // POST: Product/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add update logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: Product/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        // POST: Product/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}
