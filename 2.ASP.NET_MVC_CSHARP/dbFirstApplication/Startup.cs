﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(dbFirstApplication.Startup))]
namespace dbFirstApplication
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
