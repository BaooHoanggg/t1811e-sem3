﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace ExamTestFinal.Models
{
    public class Category
    {
        [Key]
        [Display(Name = "Category ID")]
        public int ID { get; set; }

        [Required(ErrorMessage = "Cannot leave Category Name field blank")]
        [Display(Name = "Category Name")]
        public string CategoryName { get; set; }

        [Required(ErrorMessage = "Description for this Product is required")]
        [StringLength(300, MinimumLength = 10, ErrorMessage = "Description cannot be longer than 300 and shorter than 10 characters")]
        public string Description { get; set; }

        [Required(ErrorMessage = "Cannot leave Picture Path field blank")]
        [Display(Name = "Picture Path")]
        public string Picture { get; set; }

        //connect to Product
        public virtual Product Product { get; set; }
    }
}