﻿using CodeFirstApplication.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Data.Entity.ModelConfiguration.Configuration;

namespace CodeFirstApplication.DAL
{
    public class FptAptechContext : DbContext
    {
        public FptAptechContext() :base("CoronaDie") //FptAptechContext
        {

        }


        // DbSet insides DbContext class
        public DbSet<Student> Students { get; set; }
        public DbSet<Course> Courses { get; set; }
        public DbSet<Enrollment> Enorollments { get; set; }

        // cau hinh de cho phep cap nhat database/model -> model configuration -> moi ghi cap nhat entity
        // -> ghi de entity -> override
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
            //modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();
        }


    }
}