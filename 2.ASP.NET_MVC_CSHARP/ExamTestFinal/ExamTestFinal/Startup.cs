﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(ExamTestFinal.Startup))]
namespace ExamTestFinal
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
