﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;

namespace WCF1
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "IBookServices" in both code and config file together.
    [ServiceContract]
    public interface IEmployeeServices

        /*
         * POST: Create
         * GET: Read
         * PUT: Edit
         * DELETE: Deletes
         */

    {
        [OperationContract]
        [WebInvoke(Method ="GET", RequestFormat =WebMessageFormat.Json, ResponseFormat =WebMessageFormat.Json, UriTemplate ="v1/Employees/")]
        List<Employee> GetEmployeeList();

        [OperationContract]
        [WebInvoke(Method = "POST", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "v1/AddEmployee/")]
        string AddEmployee(Employee employee);

        [OperationContract]
        [WebInvoke(Method = "GET", RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json, UriTemplate = "v1/Employee/{dept_name}/")]
        Employee GetEmployeeByDepartment(string dept_name);
    }
}
