﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using ExampleCommerce;

namespace ExampleCommerce.Controllers
{
    public class INDIVIDUALsController : Controller
    {
        private eCommerceEntities db = new eCommerceEntities();

        // GET: INDIVIDUALs
        public ActionResult Index()
        {
            var iNDIVIDUALs = db.INDIVIDUALs.Include(i => i.CUSTOMER);
            return View(iNDIVIDUALs.ToList());
        }

        // GET: INDIVIDUALs/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            INDIVIDUAL iNDIVIDUAL = db.INDIVIDUALs.Find(id);
            if (iNDIVIDUAL == null)
            {
                return HttpNotFound();
            }
            return View(iNDIVIDUAL);
        }

        // GET: INDIVIDUALs/Create
        public ActionResult Create()
        {
            ViewBag.CUST_ID = new SelectList(db.CUSTOMERs, "CUST_ID", "ADDRESS");
            return View();
        }

        // POST: INDIVIDUALs/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "BIRTH_DATE,FIRST_NAME,LAST_NAME,CUST_ID")] INDIVIDUAL iNDIVIDUAL)
        {
            if (ModelState.IsValid)
            {
                db.INDIVIDUALs.Add(iNDIVIDUAL);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.CUST_ID = new SelectList(db.CUSTOMERs, "CUST_ID", "ADDRESS", iNDIVIDUAL.CUST_ID);
            return View(iNDIVIDUAL);
        }

        // GET: INDIVIDUALs/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            INDIVIDUAL iNDIVIDUAL = db.INDIVIDUALs.Find(id);
            if (iNDIVIDUAL == null)
            {
                return HttpNotFound();
            }
            ViewBag.CUST_ID = new SelectList(db.CUSTOMERs, "CUST_ID", "ADDRESS", iNDIVIDUAL.CUST_ID);
            return View(iNDIVIDUAL);
        }

        // POST: INDIVIDUALs/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "BIRTH_DATE,FIRST_NAME,LAST_NAME,CUST_ID")] INDIVIDUAL iNDIVIDUAL)
        {
            if (ModelState.IsValid)
            {
                db.Entry(iNDIVIDUAL).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.CUST_ID = new SelectList(db.CUSTOMERs, "CUST_ID", "ADDRESS", iNDIVIDUAL.CUST_ID);
            return View(iNDIVIDUAL);
        }

        // GET: INDIVIDUALs/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            INDIVIDUAL iNDIVIDUAL = db.INDIVIDUALs.Find(id);
            if (iNDIVIDUAL == null)
            {
                return HttpNotFound();
            }
            return View(iNDIVIDUAL);
        }

        // POST: INDIVIDUALs/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            INDIVIDUAL iNDIVIDUAL = db.INDIVIDUALs.Find(id);
            db.INDIVIDUALs.Remove(iNDIVIDUAL);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
