namespace DataAnnotations.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddColumnDOB : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Employees", "DOB", c => c.String(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Employees", "DOB");
        }
    }
}
