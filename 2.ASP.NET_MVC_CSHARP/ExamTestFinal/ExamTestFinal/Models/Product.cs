﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace ExamTestFinal.Models
{
    public enum ReorderLevel
    {
        High,Medium,Low
    }

    public class Product
    {
        [Key]
        [Display(Name = "Product ID")]
        public int ProductID { get; set; }

        [Required(ErrorMessage = "Cannot leave Name field blank")]
        [StringLength(30, ErrorMessage = "Name cannot be longer than 30 characters")]
        [Display(Name = "Product Name")]
        public string ProductName { get; set; }

        [Required(ErrorMessage = "Cannot leave ID of Supplier field blank")]
        [Display(Name = "Supplier ID")]
        public int SupplierID { get; set; }

        [Required(ErrorMessage = "Cannot leave Quantity field blank")]
        [Range(5, 200, ErrorMessage = "Quantity cannot be less than 5 and more than 200")]
        public int Quantity { get; set; }

        [Required(ErrorMessage = "Cannot leave Price field blank")]
        [Range(10, 1000000, ErrorMessage = "Price per unit cannot be less than 10 and more than 1000000")]
        [Display(Name = "Price Per Unit ($)")]
        public decimal Price { get; set; }

        [Display(Name = "Units In Stock")]
        [Required(ErrorMessage = "Cannot leave Unit In Stock field blank")]
        public int UnitInStock { get; set; }

        [Display(Name = "Units In Order")]
        [Required(ErrorMessage = "Cannot leave Unit In Order field blank")]
        public int UnitInOrder { get; set; }

        [Display(Name = "Re-order Level")]
        [Required(ErrorMessage = "Cannot leave Re-order Level field blank")]
        public ReorderLevel? ReorderLevel { get; set; }

        
        public string Discontinued { get; set; }

        //Foreign key
        public int? CategoryID { get; set; }

        //connect to Category
        public virtual ICollection<Category> Categories { get; set; }
    }
}