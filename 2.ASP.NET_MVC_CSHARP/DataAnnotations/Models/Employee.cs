﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel;

namespace DataAnnotations.Models
{
    public class Employee
    {
        [Key]
        public int Id { get; set; }

        [DisplayName("Employee Name")]
        [Required(ErrorMessage ="Please enter your employee name")]
        [StringLength(35, MinimumLength = 3)]
        public string Name { get; set; }

        [Required(ErrorMessage ="Please enter your employee address")]
        [StringLength(300)]
        public string Address { get; set; }

        [Required(ErrorMessage ="Please enter your employee salary")]
        [Range(3000,100000, ErrorMessage ="Your employee salary must be between 3000 and 100000")]
        public decimal Salary { get; set; }

        [Required(ErrorMessage ="Please enter your employee DOB")]
        public string DOB { get; set; }

        [DisplayName("Email Adress")]
        [Required(ErrorMessage ="Please enter your employee email address")]
        [DataType(DataType.EmailAddress)]
        [MaxLength(50)]
        [RegularExpression(@"[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}", ErrorMessage ="Please enter valid email")]
        public string Email { get; set; }

    }
}