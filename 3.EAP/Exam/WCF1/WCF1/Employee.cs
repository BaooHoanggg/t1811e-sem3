﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace WCF1
{
    [DataContract]
    public class Employee
    {
        [DataMember]
        public int EmployeeId { get; set; }

        [DataMember]
        public string Name { get; set; }

        [DataMember]
        public int Salary { get; set; }

        [DataMember]
        public string Department { get; set; }

    }

    public interface IEmployeeRepository
    {
        List<Employee> GetAllEmployee(); 
        
        Employee GetEmployeeByDepartment(string dept_name);
        
        Employee AddNewEmployee(Employee item);

    }

    public class EmployeeRepository : IEmployeeRepository
    {
        private List<Employee> emps = new List<Employee>();
        private int counter = 1;

        /**
        public EmployeeRepository()
        {
            AddNewEmployee(new Employee() { Name = "Nguyen Van A", Salary = 1111, Department = "A" });
            AddNewEmployee(new Employee() { Name = "Tran Van B", Salary = 2222, Department = "B" });
            AddNewEmployee(new Employee() { Name = "Nguyen Thi C", Salary = 3333, Department = "C" });
        }
    **/

        public Employee AddNewEmployee(Employee item)
        {
            if (item == null)
            {
                throw new AggregateException("newEmployee");
            }

            item.EmployeeId = counter++;
            emps.Add(item);
            return item;
        }     

        public List<Employee> GetAllEmployee()
        {
            return emps;
        }

        public Employee GetEmployeeByDepartment(string dept_name)
        {
            return emps.Find(e => e.Department.Contains(dept_name));
        }

    }
}