﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CodeFirstApplication.Models
{
    public class Enrollment
    {
        public int EnrollmentId { get; set; }
        public int Grade { get; set; }

        // Reference key
        public int StudentId { get; set; }
        public int CourseId { get; set; }

        // Mapping
        public virtual Student Student { get; set; }
        public virtual Course Course { get; set; }

    }
}