﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BindingData.Models
{
    public class Book
    {
        public int BookId { get; set; }
        public string Title { get; set; }
        public string Author { get; set; }
        public string CoverImage { get; set; }

    }

    public class BookManager
    {
        public static List<Book> GetBooks()
        {
            var books = new List<Book>();

            books.Add(new Book { BookId = 1, Title = "Hitler's other half1", Author = "Eric-Emmanuel Schmitt", CoverImage = "Assets/1.png" });
            books.Add(new Book { BookId = 2, Title = "Hitler's other half2", Author = "Eric-Emmanuel Schmitt", CoverImage = "Assets/2.png" });
            books.Add(new Book { BookId = 3, Title = "Hitler's other half3", Author = "Eric-Emmanuel Schmitt", CoverImage = "Assets/3.png" });
            books.Add(new Book { BookId = 4, Title = "Hitler's other half4", Author = "Eric-Emmanuel Schmitt", CoverImage = "Assets/4.png" });
            books.Add(new Book { BookId = 5, Title = "Hitler's other half5", Author = "Eric-Emmanuel Schmitt", CoverImage = "Assets/5.png" });
            books.Add(new Book { BookId = 6, Title = "Hitler's other half6", Author = "Eric-Emmanuel Schmitt", CoverImage = "Assets/6.png" });
            books.Add(new Book { BookId = 7, Title = "Hitler's other half7", Author = "Eric-Emmanuel Schmitt", CoverImage = "Assets/7.png" });
            books.Add(new Book { BookId = 8, Title = "Hitler's other half8", Author = "Eric-Emmanuel Schmitt", CoverImage = "Assets/8.png" });
            books.Add(new Book { BookId = 9, Title = "Hitler's other half9", Author = "Eric-Emmanuel Schmitt", CoverImage = "Assets/9.png" });
            books.Add(new Book { BookId = 10, Title = "Hitler's other half10", Author = "Eric-Emmanuel Schmitt", CoverImage = "Assets/10.png" });
            books.Add(new Book { BookId = 11, Title = "Hitler's other half11", Author = "Eric-Emmanuel Schmitt", CoverImage = "Assets/11.png" });
            books.Add(new Book { BookId = 12, Title = "Hitler's other half12", Author = "Eric-Emmanuel Schmitt", CoverImage = "Assets/12.png" });
            books.Add(new Book { BookId = 13, Title = "Hitler's other half13", Author = "Eric-Emmanuel Schmitt", CoverImage = "Assets/13.png" });


            return books;
        }

    }
}
