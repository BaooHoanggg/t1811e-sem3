﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace WCF1
    //Should not code here
    //just call method from another class -> interface -> class khac goi interface
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "BookServices" in code, svc and config file together.
    // NOTE: In order to launch WCF Test Client for testing this service, please select BookServices.svc or BookServices.svc.cs at the Solution Explorer and start debugging.
    public class BookServices : IEmployeeServices
    {
        static IEmployeeRepository repository = new EmployeeRepository();
        public string AddEmployee(Employee employee)
        {
            Employee newEmployee = repository.AddNewEmployee(employee);
            return "id = " + newEmployee.EmployeeId;
        }

        public Employee GetEmployeeByDepartment(string dept_name)
        {
            return repository.GetEmployeeByDepartment(dept_name);
        }
        
        public List<Employee> GetEmployeeList()
        {
            return repository.GetAllEmployee();
            
        }

    }
}
