﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using BookClientConsumer.ServiceReference1;

namespace BookClientConsumer.Models
{
    public class EmployeeServiceClient
    {
        BookClientConsumer.ServiceReference1.EmployeeServicesClient client = new BookClientConsumer.ServiceReference1.EmployeeServicesClient();

        //EmployeeServiceClient client = new EmployeeServiceClient();

        public List<Employee> GetAllEmployee()
        {
            var list = client.GetEmployeeList().ToList();
            var rt = new List<Employee>();

            list.ForEach(l => rt.Add(new Employee() 
            { 
                EmployeeId = l.EmployeeId, 
                Name = l.Name, 
                Salary = l.Salary,
                Department = l.Department
            }));

            return rt;
        }

        public string AddAEmployee(Employee newEmployee)
        {
            var serviceEmployee = new ServiceReference1.Employee() 
            { 
                EmployeeId = newEmployee.EmployeeId, 
                Name = newEmployee.Name, 
                Salary = newEmployee.Salary,
                Department = newEmployee.Department
            };

            return client.AddEmployee(serviceEmployee);
        }

    }
}