﻿using BookClientConsumer.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace BookClientConsumer.Controllers
{
    public class EmployeeController : Controller
    {
        //Step 2: Call model
        EmployeeServiceClient employeeService = new EmployeeServiceClient();

        // GET: Book
        public ActionResult Index(/**string searchString**/)
        {
            ViewBag.listEmployees = employeeService.GetAllEmployee();

            //var employees = from e in employeeService.Employee select e;

            //if (!String.IsNullOrEmpty(searchString))
            //{
            //    employees = employees.Where(e => e.Name.Contains(searchString));
            //}

            return View();
        }

        // GET: Book/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

        // GET: Book/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Book/Create
        [HttpPost]
        public ActionResult Create(Employee employee)
        {
            //employeeService.AddAEmployee(employee);
            //return RedirectToAction("Index", "Employee");
            return View();
        }

    }
}
