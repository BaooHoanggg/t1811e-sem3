﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CodeFirstApplication.Models
{
    public class Student
    {
        public int Id { get; set; }
        public string LastName { get; set; }
        public string FirstName { get; set; }
        public string Address { get; set; }
        public DateTime EnrollmentDate { get; set; }

        // 1 Student can have many Enrollments -> Student class mapping to Enrollment class
        public virtual ICollection<Enrollment> Enrollments { get; set; }

    }
}